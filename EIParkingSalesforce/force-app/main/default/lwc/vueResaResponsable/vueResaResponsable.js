import { LightningElement, wire } from 'lwc';
import { loadScript } from  'lightning/platformResourceLoader';
import { refreshApex } from '@salesforce/apex';
import { getPicklistValues, getObjectInfo } from 'lightning/uiObjectInfoApi';

import getPlaces from '@salesforce/apex/vueResponsableController.getPlaces';
import getResaByDate from '@salesforce/apex/vueResponsableController.getResaByDate';
import MOMENT_RESOURCE from '@salesforce/resourceUrl/moment_with_locales';

import RESERVATION_OBJECT from '@salesforce/schema/Reservation__c';
import HORAIRE_FIELD from '@salesforce/schema/Reservation__c.Plage_horaire__c'

export default class VueResaResponsable extends LightningElement {
    places = [];
    joursSemaine = [];
    reservations = [];
    resaARafraichir = [];
    workingDays = [];
    resaTypeId;
    plageHoraire = [];

    connectedCallback(){
        Promise.all([
            loadScript(this, MOMENT_RESOURCE)
        ]).then(() => {
            this.workingDaysFunction();
            debugger;
        })
        .catch(error => {
            debugger;
        });
    }
    
    @wire(getPlaces)
    wiredPlaces({error, data}) {
        if (error) {
            this.error = error;
        } else if (data) {
            this.places = data;
        }
    }

    @wire(getResaByDate)
    wiredReservation(value){
        this.resaARafraichir = value;
        const {error, data} = value;
        if (error) {
            this.error = error;
            console.log(error);
        } else if (data) {
            this.reservations = data;
        }
    }

    @wire (getObjectInfo, {objectApiName: RESERVATION_OBJECT})
    wiredInfos({data, error}) {
        if (error) {
            console.log(error);
        } else if (data) {
            var resaInfo = data;
            const RTIS = resaInfo.recordTypeInfos;
            this.resaTypeId = Object.keys(RTIS)[0];
        }
    }

    @wire(getPicklistValues, {recordTypeId: '$resaTypeId', fieldApiName: HORAIRE_FIELD})
    wiredHoraire({data, error}) {
        if (error) {
            console.log(error);
        } else if (data) {
            this.plageHoraire = data.values;
        }
    }

    get nbPlacesLibres() {
        refreshApex(this.resaARafraichir);
        var nbResasParJour = 0;
        var nbPlacesLibres = [];
        for (var i = 0; i < this.joursSemaine.length; i++) {
            for (var j = 0; j < this.reservations.length; j++) {
                if (this.reservations[j].Date__c == this.joursSemaine[i]) {
                    nbResasParJour++;
                    // if (this.reservations[j].Plage_horaire__c == "Journée entière") {
                    //     nbResasParJour++;
                    // } else {
                    //     nbResasParJour += 0.5;
                    // }
                }
            }
            var nb = this.places.length - nbResasParJour;
            nbPlacesLibres.push(nb);
            nbResasParJour = 0;            
        }
        return nbPlacesLibres;
    }

    workingDaysFunction() {
        moment.locale('fr');
        var current = new Date();
        this.joursSemaine = [];
        var monday = current.getDate() - current.getDay() + 1;
        var date = new Date(current.setDate(monday));      
        for(var i = 0; i <= 4; i++) {
            date = moment(current).add(i, 'd').toDate();
            this.joursSemaine.push(moment(date).format("YYYY-MM-DD"));
            this.workingDays.push(moment(date).format("ddd DD/MM"));
        }
    }
}