import { LightningElement, track, wire } from 'lwc';
import Id from '@salesforce/user/Id';

import { createRecord, deleteRecord, getRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';


import { loadScript } from  'lightning/platformResourceLoader';
import { refreshApex } from '@salesforce/apex';

import RESERVATION_OBJECT from '@salesforce/schema/Reservation__c';
import NAME_FIELD from '@salesforce/schema/Reservation__c.Name';
import DATE_FIELDS from '@salesforce/schema/Reservation__c.Date__c';
import AFFECTATION_FIELDS from '@salesforce/schema/Reservation__c.UserPlaceParking__c';

import getAffectation from '@salesforce/apex/UserParking.getAffectation';
import getReservationByUser from '@salesforce/apex/ReservationByUser.getReservationByUser';

import resourceName  from '@salesforce/resourceUrl/moment_with_locales';

const USER_FIELDS = ['User.isProprietaire__c'];

export default class calendrierParking extends LightningElement {

    workingDays = [];
    copyWorkingsDays = [];
    firstWeek = true;
    workingDaysDateComplet = [];
    ReservedDatesResult;

    idUser = Id;
    userIsProprio;
    parkingsByUser=[];
    placeId;

    tabForGetChecked=[];
    dateForDeleted;

    ReservedDates=[];
    annulReserved=[];

    showErrorBloq = false;

    @track reservedRecord = {
        Name : NAME_FIELD,
        Date__c : DATE_FIELDS,
        UserPlaceParking__c : AFFECTATION_FIELDS,
    };

    connectedCallback() {
       this.init();
       refreshApex(this.ReservedDatesResult); 
       
    }
    
    renderedCallback() {
        console.log('rendered');
        refreshApex(this.ReservedDatesResult); 
        
        this.tabForGetChecked = [];

        Array.from(this.template.querySelectorAll('lightning-input'))
                    .forEach(element => {
                        element.checked = false;
                        // POUR GESTION
                        element.disabled = false;
                        if (this.userIsProprio && new Date().getDay() != 4) {
                            element.disabled = true;
                        }
                    });
        this.getDateChecked();        
        
    }

    init = async () => {
        try {
            await loadScript(this, resourceName); 
            moment.locale("fr");  
            //this.workingDays = this.loadWorkingDays();         
        } catch (error) {
            console.error(error);
        } finally {
            console.log('Loaded');
        }
    }

    @wire(getRecord, {recordId : '$idUser', fields : USER_FIELDS})
    wiredInfo({error, data}) {
        if (error) {
            let message = 'Unknown error';
            if (Array.isArray(error.body)) {
                message = error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                message = error.body.message;
            }
            console.log(message);
        } else if (data) {
            this.userIsProprio = data.fields.isProprietaire__c.value;
        }
    }

    @wire(getAffectation, { idUser: '$idUser' })
    wiredParkingsByUser({ error, data }) {
        console.log('begin wiredParkingsByUser');
        if (error) {
            this.error = error;
        } else if (data) {
            this.parkingsByUser = data;
            if(this.parkingsByUser.length === 0 ){
                this.showErrorBloq = true ;
            } else {
                this.placeId = this.parkingsByUser[0].Place_de_parking__r.Id;
            };            
        }
        
        console.log('end wiredParkingsByUser');
    }
    
    
    @wire(getReservationByUser, { idPlace: '$placeId' })
    wiredReservationByUser(result) {
        this.ReservedDatesResult = result;
        console.log('begin wiredReservationByUser');    
        if (result.error) {
            this.error = error;
        } else if (result.data) {
            this.ReservedDates = result.data;
            this.workingDays = this.loadWorkingDays();
             
        }
        console.log('end wiredReservationByUser');
    }

    switchPage() {
        if(this.firstWeek){

            this.firstWeek = false;
            this.workingDays = this.copyWorkingsDays.slice(5, this.copyWorkingsDays.size);
            
            
        } else {
            this.firstWeek = true;
            this.workingDays = this.copyWorkingsDays.slice(0, 5);
            
        }
        
    }
    
    loadWorkingDays() {
        var current = new Date(); 
        var monday = current.getDate() - current.getDay() +1;
        date = new Date(current.setDate(monday));
        this.workingDaysDateComplet = [];
        this.workingDays = [];
        this.workingDaysDateComplet.push(date);             
        this.workingDays.push(moment(date).format('ddd DD/MM')); 
        for(var i=1;i<=11;i++) {
            var date;
            if(i!=5 && i!=6)
            //if(i!=0) //Monday in 0 added
            {
                date = moment(current).add(i, 'days').toDate();
                this.workingDaysDateComplet.push(date);            
                this.workingDays.push(moment(date).format('ddd DD/MM')); 
            }

        }
        this.copyWorkingsDays = this.workingDays;
        if(this.firstWeek){
            return this.copyWorkingsDays.slice(0,5);
        } else  {
            return this.copyWorkingsDays.slice(5, this.copyWorkingsDays.size);
        }

        
    }

    getDateChecked(){
        console.log('dans getDateChecked');
        console.log('dates réservées :', this.ReservedDates);
        for(var i=0;i<this.ReservedDates.length;i++) {
            var workingDaysDateCompletByWeek = workingDaysDateComplet;
            if(this.firstWeek == true) {

                this.workingDaysDateCompletByWeek = this.workingDaysDateComplet.slice(0, 5);
            } else {
                
                this.workingDaysDateCompletByWeek = this.workingDaysDateComplet.slice(5, this.workingDaysDateComplet.size);


            }
            for(var j=0;j<this.workingDaysDateCompletByWeek.length;j++) {
               
                /*Array.from(this.workingDaysDateCompletByWeek)
                .forEach(d => console.log("Date",d));
                */
                if(moment(this.workingDaysDateCompletByWeek[j]).format('YYYY-MM-DD') === this.ReservedDates[i].Date__c){
                    Array.from(this.template.querySelectorAll('lightning-input'))
                    .filter(element => element.value == j)
                    .forEach(element => {
                        element.checked = true;
                        // POUR LA GESTION
                        if (this.ReservedDates[i].UserPlaceParking__r.User__r.Id === this.idUser) {
                            element.disabled = false
                        } else if (this.ReservedDates[i].UserPlaceParking__r.User__r.Id != this.idUser) {
                            element.disabled = true;
                        }
                    });
                   
                }
            }
        }        
    }

    getChecked(e){
        var incrWeek = 0;
        if(!this.firstWeek) {
            incrWeek = 5;
        }
        if (e.target.checked === true){
            
            this.tabForGetChecked.push(this.workingDaysDateComplet[parseInt(e.target.value)+incrWeek]);

        }
        else{
            this.dateForDeleted = this.workingDaysDateComplet[parseInt(e.target.value)+incrWeek] ;

            for( var i = 0; i <this.tabForGetChecked.length; i++){
                 if ( this.tabForGetChecked[i] === this.dateForDeleted) {

                    this.tabForGetChecked.splice(i, 1);
                }
            }
            console.log('begin deletion');
            for (var j = 0; j <this.ReservedDates.length; j++) {

                if(this.ReservedDates[j].Date__c == moment(this.dateForDeleted).format('YYYY-MM-DD') ) {
                    this.annulReserved.push(this.ReservedDates[j]);
                    //this.annulReserved.forEach(c => console.log(c));
                }
            }
            console.log('end deletion');

        }
    }

    handleClick(e) {
        this.parkingsByUser.forEach(element => {
            this.reservedRecord.Name = "Réservation pour " + element.User__r.Name;
            this.reservedRecord.UserPlaceParking__c = element.Id;
            this.namePlaceParking = element.Place_de_parking__r.Name;
         
        });
        console.log('begin tabForGetChecked');
        /*Array.from(this.tabForGetChecked)
        .forEach(cc => console.log(cc));
        */
        if(this.tabForGetChecked.length === 0){

            this.annulReserved.forEach(annul => {
                deleteRecord(annul.Id)
                .then(() => {
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Success',
                            message: 'La réservation a bien été supprimée',
                            variant: 'success'
                        })
                    );
                })
                .catch(error => {
                   
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Error creating record',
                            message: error.body.message,
                            variant: 'error',
                        }),
                    );
                })
                ;});
        }else {
            console.log('end tabForGetChecked');
            this.annulReserved.forEach(annul => {
                deleteRecord(annul.Id);
                //this.annulReserved = [];
                
            });
        }
      
        this.annulReserved = [];
        for(var i=0;i<this.tabForGetChecked.length;i++) {
            const fields = {};
            fields[NAME_FIELD.fieldApiName] = this.reservedRecord.Name + " le " + moment(this.tabForGetChecked[i]).format('YYYY-MM-DD');                               
            fields[DATE_FIELDS.fieldApiName] = moment(this.tabForGetChecked[i]).format('YYYY-MM-DD');
            fields[AFFECTATION_FIELDS.fieldApiName] = this.reservedRecord.UserPlaceParking__c;

            const recordInput = { apiName: RESERVATION_OBJECT.objectApiName, fields };
            console.log('heeere');
            
            createRecord(recordInput)
                .then(result => {
                    this.reservedRecord = {};

                console.log('result ===> '+result);
                this.tabForGetChecked = [];
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Success',
                            message: 'Votre réservation est prise en compte',
                            variant: 'success',
                        }),
                    );

                })
                .catch(error => {
                   
                    this.dispatchEvent(
                        new ShowToastEvent({
                            title: 'Error creating record',
                            message: error.body.message,
                            variant: 'error',
                        }),
                    );
                });
        }

    }

}