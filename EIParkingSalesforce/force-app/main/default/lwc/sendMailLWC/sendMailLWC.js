import { LightningElement, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import sendMail from '@salesforce/apex/SendMailController.sendMail';

export default class SendMailLWC extends LightningElement {
    sujet;
    msgAtt;

    get options() {
        return [
            { label: 'Signaler une plaque', value: 'Denoncer' },
            { label: 'Demander une place', value: 'Demander' }
        ];
    }
    
    get isDenoncer() {
        return this.sujet == 'Denoncer';
    }
    
    get isDemander() {
        return this.sujet == 'Demander';
    }

    setSujet(event) {
        this.sujet = event.detail.value;
    }

    setMsgAtt(event) {
        this.msgAtt = event.target.value;
    }
    
    formatPlaque(event) {
        this.msgAtt = this.msgAtt.trim()
            .replace(/\s+/g, '')
            .replace(/-+/g, '')
            .toUpperCase();
        event.target.value = this.msgAtt;
    }

    clickSend() {
        sendMail({sujet: this.sujet, corps: this.msgAtt})
        .then(result => {
            if (result == true) {
                const evt = new ShowToastEvent({
                    message: 'Le message a bien été envoyé',
                    variant: 'success',
                });
                this.dispatchEvent(evt);
                this.sujet = undefined;
            } else if (result == false) {
                const evt = new ShowToastEvent({
                    message: 'Il y a eu un problème, le message n\'a pas pu être envoyé',
                    variant: 'error',
                });
                this.dispatchEvent(evt);
            }
        })
        .catch(error => {
            console.log(error);
        })
    }
}