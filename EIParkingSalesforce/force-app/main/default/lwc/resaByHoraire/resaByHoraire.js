import { LightningElement, api, wire } from 'lwc';
import { deleteRecord, createRecord } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';

import getUserByPlace from '@salesforce/apex/vueResponsableController.getUserByPlace';

import RESERVATION_OBJECT from '@salesforce/schema/Reservation__c';
import NAME_FIELD from '@salesforce/schema/Reservation__c.Name';
import DATE_FIELD from '@salesforce/schema/Reservation__c.Date__c';
import HORAIRE_FIELD from '@salesforce/schema/Reservation__c.Plage_horaire__c'
import AFFECTATION_FIELD from '@salesforce/schema/Reservation__c.UserPlaceParking__c';

export default class ResaByHoraire extends LightningElement {
    @api place;
    @api jour;
    @api resaARafraichir;
    @api reservations = [];
    @api horaire;
    
    placeId;
    userName;
    checkVal;
    searchView;
    affectations = [];
    utilisateurs = [];
    affectation;
    recordUserName;
    reservationId;

    connectedCallback() {
        this.placeId = this.place.Id;
        this.reservations = this.resaARafraichir.data;
        this.setUserName();
    }

    @wire(getUserByPlace, {placeId: '$placeId'})
    wiredAffectation(value) {
        this.affectations = value;
        const {data, error} = value;
        if(error) {
            console.log(error);
        } else if (data) {
            data.forEach(aff => {
                this.utilisateurs.push({value: aff.Id, label: aff.User__r.Name});
            });
        }
    }

    handleCheck(event) {
        this.searchView = event.target.checked;
        if (event.target.checked == false) {
            if (this.userName != "") {
                if (confirm("Êtes-vous sûrs de vouloir supprimer cette réservation ?")) {
                    this.checkedVal = event.target.checked;
                    deleteRecord(this.reservationId)
                    .then(() => {
                        this.dispatchEvent(
                            new ShowToastEvent({
                                title: 'Success',
                                message: 'La réservation a bien été supprimée',
                                variant: 'success'
                            })
                        );
                        this.refreshResa();
                    })
                    .catch(error => {
                        this.dispatchEvent(
                            new ShowToastEvent({
                                title: 'Erreur durant la suppression de l\'enregistrement',
                                message: error.body.message,
                                variant: 'error'
                            })
                        );
                    });
                } else {
                    this.checkedVal = true;
                }
            }
        } else if (event.target.checked == true) {
            refreshApex(this.affectations);
        }
    }
        
    handleChange(event) {
        this.affectation = event.target.value;
        this.recordUserName = event.target.options.find(opt => opt.value === event.detail.value).label;
    }
            
    handleClick() {
        const FIELDS = {};
        FIELDS[NAME_FIELD.fieldApiName] = "Réservation pour " + this.recordUserName
        + " le " + this.jour;
        FIELDS[DATE_FIELD.fieldApiName] = this.jour;
        FIELDS[AFFECTATION_FIELD.fieldApiName] = this.affectation;
        //FIELDS[HORAIRE_FIELD.fieldApiName] = this.horaire;
        const RECORD_INPUT = { apiName: RESERVATION_OBJECT.objectApiName, fields: FIELDS };

        createRecord(RECORD_INPUT)
        .then(() => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success',
                    message: 'Votre réservation est enregistrée',
                    variant: 'success',
                }),
            );
            this.searchView = false;
            this.refreshResa();
        })
        .catch(error => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error creating record',
                    message: error.body.message,
                    variant: 'error',
                })
            );
        });
    }

    setUserName() {
        for (var i = 0; i < this.reservations.length; i++) {
            if (this.reservations[i].Date__c == this.jour &&
                this.reservations[i].UserPlaceParking__r.Place_de_parking__r.Id == this.placeId /*&&
                this.reservations[i].Plage_horaire__c == this.horaire*/) {
                    this.checkVal = true;
                    this.reservationId = this.reservations[i].Id;
                    this.userName = this.reservations[i].UserPlaceParking__r.User__r.Name;
                    break;
            } else {
                this.userName = "";
            }
        }
    }

    refreshResa() {
        refreshApex(this.resaARafraichir);
        this.reservations = this.resaARafraichir.data;
        this.setUserName();
        console.log('userName : ' + this.userName);
    }
}