import { LightningElement, api } from 'lwc';

export default class AfficherReservations extends LightningElement {
    @api place;
    @api jour;
    @api reservations = [];
    @api plageHoraire = [];
}