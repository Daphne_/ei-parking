@isTest
public class TestAutoCheckisActif {
    
    @isTest
    public static void TestNominalAutoCheckisActif() {
        
        // Création avertissement 1 sur un utilisateur avec case isActif non cochée
        Avertissement__c warning = new Avertissement__c();
        warning.Utilisateur__c = '0053N000000OkkIQAS';
        warning.isActif__c = false;
        warning.dateAvertissement__c = Date.newInstance(2020, 04, 01);

        // Création avertissement 2 sur le même user et dans la même semaine
        Avertissement__c warning2 = new Avertissement__c();
        warning2.Utilisateur__c = '0053N000000OkkIQAS';
        warning2.dateAvertissement__c = Date.newInstance(2020, 04, 02); 
		
        Test.startTest();
        // Insertion avertissement 1
        try{
            insert warning;
        }catch (DmlException e) {
            System.debug(e.getMessage());
        }
        
        // Insertion avertissement 2
        try{
            insert warning2;
        }catch (DmlException e2) {
            System.debug(e2.getMessage());
        }   
        Test.stopTest();
        // Requete SOQL pour recuperer avertissement inserer
    	warning2 = [SELECT isActif__c FROM Avertissement__c WHERE dateAvertissement__c =:warning2.dateAvertissement__c]; 
        System.assertEquals(true, warning2.isActif__c);     
    }
}