@isTest
private with sharing class TestReservationByUser {
    @isTest static void TestNominal() {
        String affectation = 'a1p290000019pvxAAA' ;
        String idUser = '0051B00000Die5zQAB' ;
        
        Reservation__c reservation = new Reservation__c(UserPlaceParking__c = affectation ,Date__c = Date.newInstance(2020, 04, 07));
        insert reservation;

        List<Reservation__c> expectedReservation = new List<Reservation__c>();
        expectedReservation.add(reservation);
        
        List<Reservation__c> actualReservation = ReservationByUser.getReservationByUser(idUser);
        System.assertEquals(expectedReservation[0].Date__c, actualReservation[0].Date__c);


    }
}
