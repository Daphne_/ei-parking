@isTest
private class TestGetUserParking {
    @isTest static void TestNominal() {
        String placeParking = 'a1n29000002Ggo5AAC' ;
        String idUser = '0051B00000Die5zQAB' ;

        UserPlaceParking__c affectation = new UserPlaceParking__c(Place_de_parking__c = placeParking , User__c = idUser);
        insert affectation;

        List<UserPlaceParking__c> expectedAffectation = new List<UserPlaceParking__c>();
        expectedAffectation.add(affectation);

        List<UserPlaceParking__c> actualAffectation = UserParking.getAffectation(idUser);
        System.assertEquals(expectedAffectation[0].Place_de_parking__c, actualAffectation[0].Place_de_parking__c);
        System.assertEquals(expectedAffectation[0].User__c, actualAffectation[0].User__c);


    }
}