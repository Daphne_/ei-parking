public class EnvoiEMail {
  
    @InvocableMethod(label='Envoie email' description='Envoyer un email pour notifier les utilisateurs de l état de leur place de parking.' category='Utilisateur')
      public static List<String> getUtilisateursEmail(List<ID> placeParkingId) {

        String idPlaceParking = placeParkingId[0];
        List<UserPlaceParking__c> utilisateurs = new List<UserPlaceParking__c>();

        utilisateurs = [
            SELECT User__r.Email
            FROM UserPlaceParking__c 
            WHERE Place_de_parking__r.Id =: idPlaceParking 
            AND User__r.isProprietaire__c = false
        ];
        
        Messaging.reserveSingleEmailCapacity(utilisateurs.size());
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        
        // Strings to hold the email addresses to which you are sending the email.
        List<String> toAddresses = new List<String>();
        for (UserPlaceParking__c currentUser : utilisateurs) {

          toAddresses.add(currentUser.User__r.Email);

        }

        // Assign the addresses for the To and CC lists to the mail object.
        mail.setToAddresses(toAddresses);

        // Specify the subject line for your email address.
        mail.setSubject('Informations pour la place de parking ');

        // Specify the text content of the email.
        mail.setPlainTextBody('La place de parking est réservé pour le .');

        // Send the email you have created.
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

        return new List<String>();
    }
  }