public with sharing class ReservationByUser {
    
 @AuraEnabled(cacheable=true)
    public static List<Reservation__c> getReservationByUser(String idPlace) {
       
        return [
            SELECT Date__c, UserPlaceParking__r.User__r.Id
            FROM Reservation__c 
            WHERE UserPlaceParking__r.Place_de_parking__r.Id =: idPlace
        ];
     }
}
