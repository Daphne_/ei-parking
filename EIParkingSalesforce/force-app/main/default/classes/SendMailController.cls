public with sharing class SendMailController {
    // Une méthode pour trouver un Utilisateur à partir de sa plaque
    public static User rechercherPlaque(String plaque) {
        User user = null;
        // Récupérer l'id du groupe
        List<CollaborationGroup> groupe = [SELECT Id
                                           FROM CollaborationGroup
                                           WHERE Name LIKE '%Parking%'
                                           LIMIT 1];
        List<CollaborationGroupMember> cgm = new List<CollaborationGroupMember>();
        System.debug(groupe.size());
        // Récupérer la liste des membres du groupe
        if (groupe.size() > 0) {
            cgm = [SELECT MemberId
                   FROM CollaborationGroupMember
                   WHERE CollaborationGroupId = :groupe[0].Id];            
        }
        // Parcourir la liste
        for (CollaborationGroupMember member : cgm) {
            // Récupérer l'utilisateur en cours
            User u = [SELECT Id, Name, Numero_de_plaque__c, SenderEmail
            FROM User
            WHERE Id = :member.MemberId];
            // Comparer la plaque de l'utilisateur avec la plaque donnée
            if (u.Numero_de_plaque__c == plaque) {
                // Si correspondance, on retourne l'utilisateur
                user = u;
            }
        }
        // Sinon, on retourne null;
        return user;
    }
    
    // Méthode pour envoyer un mail à partir du corps du message passé en paramètre
    @AuraEnabled
    public static Boolean sendMail(String sujet, String corps) {
        User proprietairePlaque = null;
        // On crée un mail
        String message = '';
        String objetMessage = '';
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'d.l.merck@hotmail.fr'};
        mail.setToAddresses(toAddresses);
        // Si le sujet == dénoncer une plaque
        if (sujet == 'Denoncer') {
            // On retrouve l'utilisateur à partir de la plaque
            proprietairePlaque = rechercherPlaque(corps);
            objetMessage = 'Signalement de plaque';
            // Si le propriétaire de la plaque != null
            if (proprietairePlaque != null) {
                // On crée un corps de message avec son nom, sa plaque et son adresse mail
                String nom = proprietairePlaque.Name;
                String plaque = proprietairePlaque.Numero_de_plaque__c;
                message = 'Un utilisateur du nom de ' + nom +
                ' et propriétaire de la plaque ' + plaque +
                ' s\'est garé à une place qui ne lui était pas réservée aujourd\'hui.';
            } else {
                // Sinon, on créer un message type
                message = 'Un utilisateur inconnu ayant pour plaque ' + corps +
                ' s\'est garé à une place qui ne lui était pas réservée.';  
            }
        } else {
            // Sinon, on change l'objet du message et on le donne avec le corps du mail
            objetMessage = 'Demande de place';
            // Récupération de l'utilisateur courant pour savoir qui demande une place
            message = UserInfo.getFirstName() + ' ' +
                      UserInfo.getLastName() + ' vous a adressé une demande : \n' +
                      corps;
        }        
        // On donne le sujet et le corps du mail
        mail.setSubject(objetMessage);
        mail.setPlainTextBody(message);
        // Et on envoie le mail
        Messaging.SendEmailResult[] results = Messaging.sendEmail(
            new Messaging.SingleEmailMessage[] { mail });
        return inspectResults(results);
    }
    
    private static Boolean inspectResults(Messaging.SendEmailResult[] results) {
        Boolean sendResult = true;        
        // sendEmail returns an array of result objects.
        // Iterate through the list to inspect results. 
        // In this class, the methods send only one email, 
        // so we should have only one result.
        for (Messaging.SendEmailResult res : results) {
            if (res.isSuccess()) {
                System.debug('Email sent successfully');
            }
            else {
                sendResult = false;
                System.debug('The following errors occurred: ' + res.getErrors());                 
            }
        }        
        return sendResult;
    }
}