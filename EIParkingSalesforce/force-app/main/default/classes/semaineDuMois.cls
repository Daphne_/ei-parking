public class semaineDuMois {
    public static Integer semaineDate(Date myDate) {
        Integer weekCount = 0;
        Integer startWeekResidue = 0;
        Integer endWeekResidue = 0;
         
        //Calculating startWeekResidue
        Date dt = myDate.toStartOfMonth().addDays(-1);
        Date dtFirstWeekend = dt.toStartOfWeek().addDays(6);
        startWeekResidue = dt.daysBetween(dtFirstWeekend);
         
        //Calculating endWeekResidue
        Date dtLastWeekend = myDate.toStartOfWeek().addDays(-1);
        endWeekResidue = dtLastWeekend.daysBetween(myDate);
         
        //Counting the weeks
        weekCount = (myDate.day() - (startWeekResidue + endWeekResidue))/7;
        weekCount += (startWeekResidue > 0 ? 1:0)+(endWeekResidue > 0 ? 1:0);
        System.Debug('Week Number: ' + weekCount);
        
        return weekCount;
    }

}