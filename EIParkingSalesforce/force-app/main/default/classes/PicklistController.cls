public class PicklistController {
	@AuraEnabled        
    public static List<Parking__c> getValuesIntoList(String objectType, String selectedField){
        List<Parking__c> pickListValuesList = new List<Parking__c>();
        for (Parking__c p : [SELECT Id, Name FROM Parking__c]) {
            pickListValuesList.add(p);
        }
        return pickListValuesList;
    }
}