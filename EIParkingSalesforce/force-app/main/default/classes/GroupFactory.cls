public class GroupFactory {
    public static Map<CollaborationGroup, List<CollaborationGroupMember>> groupWithMember() {
        CollaborationGroup groupe = new CollaborationGroup(Name = 'Test1', CollaborationType = 'Public');
        insert groupe;
        groupe = [SELECT Id, Name FROM CollaborationGroup WHERE Name = 'Test1'];
        List<User> users = [SELECT Id, Name, Numero_de_plaque__c, SenderEmail
                           	FROM User
                           	WHERE Name = 'Daphné Merck'];
        List<CollaborationGroupMember> cgms = new List<CollaborationGroupMember>();
        for (User u : users) {
            CollaborationGroupMember cgm = new CollaborationGroupMember();
            cgm.CollaborationGroupId = groupe.Id;
            cgm.MemberId = u.Id;
            cgms.add(cgm);
        }
        insert cgms;
        return new Map<CollaborationGroup, List<CollaborationGroupMember>>{groupe => cgms};
    }
}