@isTest
private class TestGetValuesIntoList {
    @isTest static void testNominal() {
        Parking__c p = new Parking__c(Name = 'Parking1');
        insert p;
        List<Parking__c> expectedParkings = new List<Parking__c>();
        expectedParkings.add(p);
        List<Parking__c> actualParkings = PicklistController.getValuesIntoList('', '');
        System.assertEquals(expectedParkings[0].Name, actualParkings[0].Name);
    }
}