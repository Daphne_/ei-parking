@isTest
private class TestRechercherPlaque {
    private static String plaqueExistante = 'EZ700JA';
    private static String plaqueNonExistante = 'AZ123ZA';
    private static Map<CollaborationGroup, List<CollaborationGroupMember>> mapForGroup = GroupFactory.groupWithMember();
    private static CollaborationGroup groupe;
    
    @isTest static void TestNominalRechercherPlaque() {
        Set<CollaborationGroup> groupeSet = mapForGroup.keySet();
		for(CollaborationGroup g : groupeSet) {
            groupe = g;
        }
        CollaborationGroupMember member = mapForGroup.get(groupe)[0];
        User expectedUser = [SELECT Id, Name, Numero_de_plaque__c, SenderEmail
                             FROM User
                             WHERE Id = :member.MemberId];
        User foundUser = SendMailController.rechercherPlaque(plaqueExistante);
        System.assert(foundUser != null);
        System.assertEquals(expectedUser.Id, foundUser.Id);
        System.assertEquals(expectedUser.Name, foundUser.Name);
        System.assertEquals(expectedUser.Numero_de_plaque__c, foundUser.Numero_de_plaque__c);
    }
    
    @isTest static void TestAlternatifRechercherPlaque() {
        Set<CollaborationGroup> groupeSet = mapForGroup.keySet();
		for(CollaborationGroup g : groupeSet) {
            groupe = g;
        }
        User foundUser = SendMailController.rechercherPlaque(plaqueNonExistante);
        System.assert(foundUser == null);
    }
}