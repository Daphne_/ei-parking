@isTest
private class TestSendMail {
    private static String denoncer = 'Denoncer';
    private static String plaque = 'EZ700JA';
    private static String plaqueNonExistante = 'AZ123JA';
    private static String demander = 'Demander';
    private static String message = 'bonjour';
    private static Map<CollaborationGroup, List<CollaborationGroupMember>> mapForGroup =TestGroupFactory.groupWithMember();
    private static CollaborationGroup groupe;
    
    private static CollaborationGroup setGroupe() {
        Set<CollaborationGroup> groupeSet = mapForGroup.keySet();
		for(CollaborationGroup g : groupeSet) {
            groupe = g;
        }
        return groupe;
    }

    @isTest static void TestDenoncerNominalSendMail() {
        groupe = setGroupe();
        System.assert(SendMailController.sendMail(denoncer, plaque));
    }

    @isTest static void TestDenoncerAltSendMail() {
        groupe = setGroupe();
        System.assert(SendMailController.sendMail(denoncer, plaqueNonExistante));
    }

    @isTest static void TestDemanderSendMail() {
        groupe = setGroupe();
        System.assert(SendMailController.sendMail(demander, message));
    }
}
