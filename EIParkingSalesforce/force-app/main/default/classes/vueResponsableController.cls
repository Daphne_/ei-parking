public with sharing class vueResponsableController {
    @AuraEnabled(cacheable=true)
    public static List<Places_Parking__c> getPlaces(){
        return [
            SELECT Id, Name
            FROM Places_Parking__c
            ORDER BY Name
        ];
    }

    @AuraEnabled(cacheable=true)
    public static List<Reservation__c> getResaByDate(){
        return [
            SELECT Id, Date__c, Plage_horaire__c,
                   UserPlaceParking__r.User__r.Id, UserPlaceParking__r.User__r.Name,
                   UserPlaceParking__r.Place_de_Parking__r.Id 
            FROM Reservation__c
            WHERE Date__c = THIS_WEEK
        ];
    }

    @AuraEnabled(cacheable=true)
    public static List<UserPlaceParking__c> getUserByPlace(Id placeId){
        return [
            SELECT User__r.Id, User__r.Name
            FROM UserPlaceParking__c
            WHERE Place_de_parking__r.Id = :placeId
        ];
    }
}
