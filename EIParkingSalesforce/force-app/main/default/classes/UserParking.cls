
public with sharing class UserParking {
    
    @AuraEnabled(cacheable=true)
    public static List<UserPlaceParking__c> getAffectation(String idUser) {
        return [
            SELECT Name, Place_de_parking__r.Id, Place_de_parking__r.Name, Place_de_parking__r.Etage__c, User__r.Name, User__r.Id 
            FROM UserPlaceParking__c 
            WHERE User__r.Id =: idUser
            AND User__r.BloqueParking__c = false 
            LIMIT 1
        ];
     }
}
