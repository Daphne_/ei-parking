trigger OneReservationParking on Reservation__c (before insert, before update) {
    // Récupération de toutes les réservations
    List<Reservation__c> allResa = [
        SELECT Date__c, Plage_horaire__c, UserPlaceParking__r.Place_de_parking__c
        FROM Reservation__c
    ];
    
    Set<Id> affectationsId = new Set<Id>();
    
    for (Reservation__c newResa : Trigger.new) {
        affectationsId.add(newResa.UserPlaceParking__c);
    }
    // Récupération de la place liée à l'affectation de la réservation
    // puisqu'elle n'est pas disponible dans la liste Trigger.new
    List<UserPlaceParking__c> affectationNewResa = [
        SELECT Place_de_parking__c
        FROM UserPlaceParking__c
        WHERE Id IN :affectationsId
    ];
    
    // Récupération des réservations qui viennent d'être créer
    for (Reservation__c newResa : Trigger.new) {
        // Pour chaque affectation
        for (UserPlaceParking__c affectation : affectationNewResa) {
            // Et pour chaque réservation déjà dans la base de donnée
            for (Reservation__c oldResa : allResa) {
                // On compare la date
                if (newResa.Date__c == oldResa.Date__c &&
                // Et la place récupérée dans l'affectation
                    affectation.Place_de_parking__c ==
                    oldResa.UserPlaceParking__r.Place_de_parking__c &&
                    newResa.Id != oldResa.Id) {
                    newResa.addError('Un utilisateur a déjà fait une réservation sur cette place');
                    // if (oldResa.Plage_horaire__c == 'Journée entière') {
                        //     newResa.addError('Un utilisateur a déjà réservé cette place pour la journée');
                        // } else if (newResa.Plage_horaire__c == oldResa.Plage_horaire__c) {
                            //     newResa.addError('Un utilisateur a déjà réservé cette place pour cette plage horaire');
                            // }
                }
            }
        }
    }
}