trigger AutoCheckisActif on Avertissement__c (before insert) {
    for (Avertissement__c newAvrt : Trigger.New) {
        List<Avertissement__c> oldAvrt = [SELECT iD, isActif__c, dateAvertissement__c FROM Avertissement__c];
        for (Avertissement__c avrt : oldAvrt) {
            Integer weekCountOldAvrt = semaineDuMois.semaineDate(avrt.dateAvertissement__c);
            Integer weekCountNewAvrt = semaineDuMois.semaineDate(newAvrt.dateAvertissement__c);
            
            if ((weekCountOldAvrt == weekCountNewAvrt) && (!avrt.isActif__c) && 
                (avrt.dateAvertissement__c.month() == newAvrt.dateAvertissement__c.month())) {
                newAvrt.isActif__c = true;
            }
        }
    }
}