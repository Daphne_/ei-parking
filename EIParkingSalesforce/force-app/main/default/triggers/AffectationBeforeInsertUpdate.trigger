trigger AffectationBeforeInsertUpdate on UserPlaceParking__c (before insert, before update) {
    // Récupération des anciennes associations
    List<UserPlaceParking__c> assoc = [SELECT User__c, User__r.isProprietaire__c, Temporaire__c, Place_de_parking__c
                                       FROM UserPlaceParking__c];

    Set<Id> newUserId = new Set<Id>();

    // Récupération des Ids des utilisateurs pour pouvoir récupérer les champs indisponibles dans Trigger.new
    for (UserPlaceParking__c upp : Trigger.new) {
        newUserId.add(upp.User__c);
    }

    // Récupération des champs dont on a besoin pour le Trigger
    List<User> utilisateurAffecte = [
        SELECT isProprietaire__c, Numero_de_plaque__c
        FROM User
        WHERE Id IN :newUserId
    ];

    for (UserPlaceParking__c upp : Trigger.new) {
        for (User user : utilisateurAffecte) {
            // 1. On vérifie que l'utilisateur a bien un numéro de plaque d'immatriculation
            if (user.Id == upp.User__c &&
                (user.Numero_de_plaque__c == null || user.Numero_de_plaque__c == '')) {
                upp.addError('Un utilisateur ne peut être affecté à une place tant ' +
                    'qu\'il n\'a pas renseigné son numéro de plaque d\'immatriculation');
            }
        }
        for(UserPlaceParking__c alreadyAssoc : assoc) {
            // 2. On vérifie que l'utilisateur n'a pas déjà une place affectée
            if (upp.User__c == alreadyAssoc.User__c &&
                upp.Temporaire__c == false &&
                upp.Id != alreadyAssoc.Id) {
               	upp.addError('Un utilisateur ne peut être affecté qu\'à une seule place');
            }
            // 3. On vérifie que cette place n'a pas déjà été attribuée à un autre propriétaire
            // si l'utilisateur est lui-même propriétaire
            if (upp.Place_de_parking__c == alreadyAssoc.Place_de_parking__c) {
                for (User user : utilisateurAffecte) {
                    if (user.Id == upp.User__c && user.isProprietaire__c &&
                        alreadyAssoc.User__r.isProprietaire__c &&
                        upp.Id != alreadyAssoc.Id) {
                        upp.addError('Un utilisateur est déjà titulaire de cette place');
                    }
                }
            }
        }
    }
}