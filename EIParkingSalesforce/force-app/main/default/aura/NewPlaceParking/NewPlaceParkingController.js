({
  doInit: function(component, event, helper) {
    component.find("forceRecord").getNewRecord(
      "Places_Parking__c",
      null,
      false,
      $A.getCallback(function() {
        var rec = component.get("v.placeRecord");
        var error = component.get("v.recordError");
        if (error || rec === null) {
          console.log("Error initializing record template: " + error);
          return;
        }
      })
      );
    },
    saveRecord: function(component, event, helper) {
      var placeNum = parseInt(component.find("placeNum").get("v.value"), 10);
      var placeEtage = parseInt(component.find("placeEtage").get("v.value"), 10);
      if (placeNum <= 9) {
        placeNum = '0' + placeNum;
      }
      var name = "Etage " + placeEtage + " N° " + placeNum;
      console.log("Dans saveRecord");
      console.log("Name : " + name);
      component.set("v.placeRecord.Name", name);
      component.set("v.placeRecord.Numero__c", placeNum);
      component.set("v.placeRecord.Etage__c", placeEtage);
      var pValue = component.find("placeParking").get("v.value")
      console.log("Parking : " + pValue);
      component.set(
        "v.placeRecord.Parking__c",
        component.find("placeParking").get("v.value")
        );
        var tempRec = component.find("forceRecord");
        tempRec.saveRecord(
          $A.getCallback(function(result) {
            console.log(result.state);
            var resultsToast = $A.get("e.force:showToast");
            if (result.state === "SUCCESS") {
              var recId = result.recordId;
              helper.navigateTo(component, recId);
            } else if (result.state === "ERROR") {
              console.log("Error: " + JSON.stringify(result.error));
              resultsToast.setParams({
                title: "Error",
                message:
                "There was an error saving the record: " +
                JSON.stringify(result.error)
              });
              resultsToast.fire();
            } else {
              console.log(
                "Unknown problem, state: " +
                result.state +
                ", error: " +
                JSON.stringify(result.error)
                );
              }
            })
            );
          },
          cancelDialog : function(component, helper) {
            var homeEvt = $A.get("e.force:navigateToObjectHome");
            homeEvt.setParams({
              "scope": "Places_Parking__c"
            });
            homeEvt.fire();
          }  
        });