({
  doInit: function(component) {
    var action = component.get("c.getValuesIntoList");
    action.setParams({
      objectType: component.get("v.sObjectName"),
      selectedField: component.get("v.fieldName")
    });
    action.setCallback(this, function(response) {
      var list = response.getReturnValue();
      component.set("v.picklistValues", list);
      console.log("dans la méthode picklist");
      console.log(list);
    });
    $A.enqueueAction(action);
  }
});