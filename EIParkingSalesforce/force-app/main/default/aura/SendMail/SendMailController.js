({
  send: function(component, event, helper) {
    var action = component.get("c.sendMail");
    var sujet = component.get("v.sujet");
    var pathName = window.location.pathname;
    pathName = pathName.split("/");
    var id = pathName[pathName.length - 2];
    console.log("sujet : " + sujet);
    if (sujet == "Denoncer") {
      var plaque = component.find("plaque").get("v.value");
      console.log("plaque : " + plaque);
      action.setParams({
        sujet: sujet,
        corps: plaque,
        groupId: id
      });
    } else if (sujet == "Demander") {
      var msg = component.get("v.msgAtt");
      console.log("msg : " + msg);
      action.setParams({
        sujet: sujet,
        corps: msg,
        groupId: id
      });
    }
    console.log("est-ce que tu me vois ?");
    action.setCallback(this, function(response) {
      var state = response.getState();
      var resultsToast = $A.get("e.force:showToast");
      var navEvt = $A.get("e.force:navigateToSObject");
      var pathName = window.location.pathname;
      pathName = pathName.split("/");
      var id = pathName[pathName.length - 2];
      console.log(id);
      if (state === "SUCCESS") {
        navEvt.setParams({
          recordId: id
        });
        resultsToast.setParams({
          type: "success",
          message: "Le message a bien été envoyé"
        });
        resultsToast.fire();
        navEvt.fire();
        console.log("dans le callBack de envoyé");
      }
    });
    $A.enqueueAction(action);
  },
  formatPlaque: function(component) {
    var plaque = component.find("plaque").get("v.value");
    plaque = plaque.trim();
    plaque = plaque.replace(/\s+/g, '');
    plaque = plaque.replace(/-+/g, '');
    plaque = plaque.toUpperCase();
    component.find("plaque").set("v.value", plaque);
  }
});